package token

import (
	"varanid-server/models/form/token"
	"varanid-server/setting"
	"varanid-server/setting/resp"
	"varanid-server/setting/util"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

var (
	// AccessToken 微信令牌
	AccessToken string
)

// 获取
func GetToken(c *gin.Context)  {
	var tokenJson token.Token
	if err := c.ShouldBindJSON(&tokenJson); err != nil {
		resp.ParamsError(c)
		return
	}

	// todo 从@ly那获取token
	//AccessToken = tokenJson.AccessToken

	url :=  "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + setting.AppId + "&secret=" + setting.AppSecret
	resMap, err := util.HttpGetStringMap(&http.Client{}, url)
	if err != nil {
		fmt.Println(err)
		resp.Fail(c)
	}

	AccessToken = resMap["access_token"]
	resp.SuccessCustom(c, map[string]string{"access_token": AccessToken})
}
