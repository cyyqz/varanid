package retain

import (
	"varanid-server/models/form/retain"
	"varanid-server/service/token"
	"varanid-server/setting"
	"varanid-server/setting/resp"
	"varanid-server/setting/util"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

// GetRetain 获取用户访问小程序留存 日/周/年
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/data-analysis/visit-retain/analysis.getDailyRetain.html
func GetRetain(c *gin.Context) {
	var retainJson retain.Retain
	if err := c.ShouldBindJSON(&retainJson); err != nil {
		resp.ParamsError(c)
		return
	}

	var target string
	switch retainJson.Flag {
		case 0:
			target = "/datacube/getweanalysisappiddailyretaininfo"
		case 1:
			target = "/datacube/getweanalysisappidweeklyretaininfo"
		case 2:
			target = "/datacube/getweanalysisappidmonthlyretaininfo"
	}

	url := setting.URL + target + "?access_token=" + token.AccessToken
	body := make(map[string]interface{})
	body["begin_date"] = retainJson.BeginDate
	body["end_date"] = retainJson.EndDate

	resMap, err := util.HttpPostStringMap(&http.Client{}, url, util.MapPostBody(body))
	if err != nil {
		log.Fatalf("http 错误，url: %s, err: %s", url, err)
	}
	resp.SuccessCustom(c, resMap)
}
