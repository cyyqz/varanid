package visit

import (
	"varanid-server/models/form/trend"
	"varanid-server/service/token"
	"varanid-server/setting"
	"varanid-server/setting/resp"
	"varanid-server/setting/util"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

// GetPage 访问页面。目前只提供按 page_visit_pv 排序的 top200。
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/data-analysis/analysis.getVisitPage.html
func GetPage(c *gin.Context) {
	var trendJson trend.Trend
	if err := c.ShouldBindJSON(&trendJson); err != nil {
		resp.ParamsError(c)
		return
	}

	url := setting.URL + "/datacube/getweanalysisappidvisitpage?access_token=" + token.AccessToken
	body := make(map[string]interface{})
	body["begin_date"] = trendJson.BeginDate
	body["end_date"] = trendJson.EndDate

	resMap, err := util.HttpPostStringMap(&http.Client{}, url, util.MapPostBody(body))
	if err != nil {
		log.Fatalf("http 错误，url: %s, err: %s", url, err)
	}
	resp.SuccessCustom(c, resMap)
}
