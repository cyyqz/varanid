package performance

import (
	"varanid-server/models/form/performance"
	"varanid-server/service/token"
	"varanid-server/setting/resp"
	"varanid-server/setting/util"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

// GetPerformanceData 获取小程序启动性能，运行性能等数据。
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/data-analysis/analysis.getPerformanceData.html
func GetPerformanceData(c *gin.Context) {
	var performanceJson performance.Performance
	if err := c.ShouldBindJSON(&performanceJson); err != nil {
		resp.ParamsError(c)
		return
	}

	url := "https://api.weixin.qq.com/wxa/business/performance/boot?access_token=" + token.AccessToken
	body := make(map[string]interface{})
	body["time"] = performanceJson.Time
	body["module"] = performanceJson.Module
	body["params"] = performanceJson.Params

	resMap, err := util.HttpPostStringMap(&http.Client{}, url, util.MapPostBody(body))
	if err != nil {
		log.Fatalf("http 错误，url: %s, err: %s", url, err)
	}
	resp.SuccessCustom(c, resMap)
}
