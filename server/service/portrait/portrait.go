package portrait

import (
	"varanid-server/models/form/trend"
	"varanid-server/service/token"
	"varanid-server/setting"
	"varanid-server/setting/resp"
	"varanid-server/setting/util"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

// GetPortrait 获取小程序新增或活跃用户的画像分布数据。时间范围支持昨天、最近7天、最近30天。
// 其中，新增用户数为时间范围内首次访问小程序的去重用户数，活跃用户数为时间范围内访问过小程序的去重用户数。
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/data-analysis/analysis.getUserPortrait.html
func GetPortrait(c *gin.Context) {
	var trendJson trend.Trend
	if err := c.ShouldBindJSON(&trendJson); err != nil {
		resp.ParamsError(c)
		return
	}

	url := setting.URL + "/datacube/getweanalysisappiduserportrait?access_token=" + token.AccessToken
	body := make(map[string]interface{})
	body["begin_date"] = trendJson.BeginDate
	body["end_date"] = trendJson.EndDate

	resMap, err := util.HttpPostStringMap(&http.Client{}, url, util.MapPostBody(body))
	if err != nil {
		log.Fatalf("http 错误，url: %s, err: %s", url, err)
	}
	resp.SuccessCustom(c, resMap)
}
