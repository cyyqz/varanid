package trend

import (
	"varanid-server/models/form/trend"
	"varanid-server/service/token"
	"varanid-server/setting"
	"varanid-server/setting/resp"
	"varanid-server/setting/util"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

// GetTrend 获取用户访问小程序数据日趋势 日/周/年
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/data-analysis/visit-trend/analysis.getDailyVisitTrend.html
func GetTrend(c *gin.Context) {
	var trendJson trend.Trend
	if err := c.ShouldBindJSON(&trendJson); err != nil {
		resp.ParamsError(c)
		return
	}

	var target string
	switch trendJson.Flag {
	case 0:
		target = "/datacube/getweanalysisappiddailyvisittrend"
	case 1:
		target = "/datacube/getweanalysisappidweeklyvisittrend"
	case 2:
		target = "/datacube/getweanalysisappidmonthlyvisittrend"
	}

	url := setting.URL + target + "?access_token=" + token.AccessToken
	body := make(map[string]interface{})
	body["begin_date"] = trendJson.BeginDate
	body["end_date"] = trendJson.EndDate

	resMap, err := util.HttpPostStringMap(&http.Client{}, url, util.MapPostBody(body))
	if err != nil {
		log.Fatalf("http 错误，url: %s, err: %s", url, err)
	}
	resp.SuccessCustom(c, resMap)
}

