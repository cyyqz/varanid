package form

// Date 通用日期 请求结构
type Date struct {
	BeginDate   string `form:"begin_date" json:"begin_date" uri:"begin_date" xml:"begin_date" validate:"required"`
	EndDate string `form:"end_date" json:"end_date" uri:"end_date" xml:"end_date" validate:"required"`
}
