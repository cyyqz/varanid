package trend

import "varanid-server/models/form"

// Trend 访问趋势 请求结构体
type Trend struct {
	form.Date
	// Flag 0:日 1:周 2:月
	Flag      int8   `form:"flag" json:"flag" uri:"flag" xml:"flag" validate:"required"`
}
