package visit

import "varanid-server/models/form"

// Page 访问页面 请求结构体
type Page struct {
	form.Date
}