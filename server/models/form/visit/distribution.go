package visit

import "varanid-server/models/form"

// Distribution 用户访问分布 请求结构体
type Distribution struct {
	form.Date
}