package summary

import "varanid-server/models/form"

// Summary 日总结 请求结构体
type Summary struct {
	form.Date
}
