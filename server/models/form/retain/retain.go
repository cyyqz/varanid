package retain

import "varanid-server/models/form"

// Retain 访问留存 请求结构体
type Retain struct {
	form.Date
	// Flag 0:日 1:周 2:月
	Flag      int8   `form:"flag" json:"flag" uri:"flag" xml:"flag" validate:"required"`
}
