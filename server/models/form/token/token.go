package token

// Token access_token 请求结构体
type Token struct {
	AccessToken string `form:"access_token" json:"access_token" uri:"access_token" xml:"access_token" validate:"required"`
}
