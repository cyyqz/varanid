package portrait

import "varanid-server/models/form"

// Portrait 用户画像 请求结构体
type Portrait struct {
	form.Date
}