package performance

// Performance 小程序性能数据 请求结构体
type Performance struct {
	Time time `form:"time" json:"time" uri:"time" xml:"time" validate:"required"`
	Params []param `form:"params[]" json:"params[]" uri:"params[]" xml:"params[]" validate:"required"`
	Module string `form:"module" json:"module" uri:"module" xml:"module" validate:"required"`
}

type time struct {
	BeginTimeStamp string `form:"begin_timestamp" json:"begin_timestamp" uri:"begin_timestamp" xml:"begin_timestamp" validate:"required"`
	EndTimeStamp   string `form:"end_timestamp" json:"end_timestamp" uri:"end_timestamp" xml:"end_timestamp" validate:"required"`
}

type param struct {
	Field string   `form:"field" json:"field" uri:"field" xml:"field" validate:"required"`
	Value string   `form:"value" json:"value" uri:"value" xml:"value" validate:"required"`
}
