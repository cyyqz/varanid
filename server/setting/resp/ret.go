package resp

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Success(c *gin.Context)  {
	c.JSON(http.StatusOK, gin.H{
		"code" : SUCCESS,
		"msg" : GetMsg(SUCCESS),
		"data" : map[string]string{},
	})
}

func SuccessCustom(c *gin.Context, data interface{})  {
	c.JSON(http.StatusOK, gin.H{
		"code" : SUCCESS,
		"msg" : GetMsg(SUCCESS),
		"data" : data,
	})
}

func Fail(c *gin.Context)  {
	c.JSON(http.StatusOK, gin.H{
		"code" : ERROR,
		"msg" : GetMsg(ERROR),
		"data" : map[string]string{},
	})
}

func FailCustom(c *gin.Context, data interface{})  {
	c.JSON(http.StatusOK, gin.H{
		"code" : ERROR,
		"msg" : GetMsg(ERROR),
		"data" : data,
	})
}

func ParamsError(c *gin.Context)  {
	c.JSON(http.StatusOK, gin.H{
		"code" : INVALID_PARAMS,
		"msg" : GetMsg(INVALID_PARAMS),
		"data" : map[string]string{},
	})
}

func Custom(c *gin.Context, code int, msg string, data interface{})  {
	c.JSON(http.StatusOK, gin.H{
		"code" : code,
		"msg" : msg,
		"data" : data,
	})
}



