package setting

import (
	"log"
	"time"

	"github.com/go-ini/ini"
)

var (
	Cfg *ini.File

	// RunMode 启动模式
	RunMode string

	// HTTPPort 端口
	HTTPPort int
	// ReadTimeout 读超时时间
	ReadTimeout time.Duration
	// WriteTimeout 写超时时间
	WriteTimeout time.Duration
	// ContentType http请求ContentType
	ContentType string

	// PageSize 分页页数
	PageSize int

	// AppId wx
	AppId string
	// AppSecret wx
	AppSecret string
	// URL 请求url
	URL string
)

func init() {
	var err error
	Cfg, err = ini.Load("conf/app.ini")
	if err != nil {
		log.Fatalf("Fail to parse 'conf/app.ini': %v", err)
	}
	LoadBase()
	LoadServer()
	LoadApp()
	LoadWX()
}

func LoadBase() {
	RunMode = Cfg.Section("").Key("RUN_MODE").MustString("debug")
}

func LoadServer() {
	sec, err := Cfg.GetSection("server")
	if err != nil {
		log.Fatalf("Fail to get p 'server': %v", err)
	}
	HTTPPort = sec.Key("HTTP_PORT").MustInt(8000)
	ReadTimeout = time.Duration(sec.Key("READ_TIMEOUT").MustInt(60)) * time.Second
	WriteTimeout = time.Duration(sec.Key("WRITE_TIMEOUT").MustInt(60)) * time.Second
	ContentType = sec.Key("CONTENT_TYPE").MustString("application/json; charset=utf-8")
}

func LoadApp() {
	sec, err := Cfg.GetSection("app")
	if err != nil {
		log.Fatalf("Fail to get p 'app': %v", err)
	}
	PageSize = sec.Key("PAGE_SIZE").MustInt(10)
}

func LoadWX() {
	sec, err := Cfg.GetSection("wx")
	if err != nil {
		log.Fatalf("Fail to get p 'wx': %v", err)
	}
	AppId = sec.Key("APP_ID").MustString("隐私数据")
	AppSecret = sec.Key("APP_SECRET").MustString("隐私数据")
	URL = sec.Key("URL").MustString("https://api.weixin.qq.com")
}
