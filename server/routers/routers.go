package routers

import (
	"varanid-server/service/performance"
	"varanid-server/service/portrait"
	"varanid-server/service/retain"
	"varanid-server/service/summary"
	"varanid-server/service/token"
	"varanid-server/service/trend"
	"varanid-server/service/visit"
	"varanid-server/setting"
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.New()

	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	gin.SetMode(setting.RunMode)

	prefix := "/mockshishi/api/v1.0/dataPlatform"

	// access_token
	r.POST("/wx/getAccessToken", token.GetToken)

	// 访问留存
	r.POST(prefix + "/uv/retain", retain.GetRetain)
	// 访问趋势
	r.POST(prefix + "/uv/trend", trend.GetTrend)

	// 用户访问概览
	r.POST(prefix + "/summary/dailySummary", summary.GetDailySummary)
	r.POST(prefix + "/summary/weeklySummary", summary.GetWeeklySummary)

	// 性能数据
	r.POST(prefix + "/sys/performanceData", performance.GetPerformanceData)

	// 用户画像
	r.POST(prefix + "/user/portrait", portrait.GetPortrait)

	// 访问数据
	r.POST(prefix + "/visit/distribution", visit.GetDistribution)
	r.POST(prefix + "/visit/page", visit.GetPage)

	return r
}
